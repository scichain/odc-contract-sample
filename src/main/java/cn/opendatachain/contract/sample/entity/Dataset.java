package cn.opendatachain.contract.sample.entity;

import cn.opendatachain.common.core.domain.ODCPayload;
import lombok.Data;

/**
 * Dataset 示例
 *
 * @author 郭志斌
 * @version odc-contract-sample 1.0.0.RELEASE
 * <b>Creation Time:</b> 2023/5/26 14:51
 */
public class Dataset extends ODCPayload {

    /**
     * 数据集名称
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //更多属性信息...

}
