package cn.opendatachain.contract.sample;

import cn.opendatachain.contract.OpenDataChainContract;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OdcContractSampleApplication extends OpenDataChainContract {

    public static void main(String[] args) {
        OpenDataChainContract.run(OdcContractSampleApplication.class, args);
    }

}
