package cn.opendatachain.contract.sample;

import cn.opendatachain.common.core.ContractPayloadBuilder;
import cn.opendatachain.common.core.domain.ContractPayload;
import cn.opendatachain.common.core.domain.CustomPayload;
import cn.opendatachain.common.rsocket.ODCResponse;
import cn.opendatachain.contract.sample.entity.Dataset;
import cn.opendatachain.contract.util.JsonUtils;
import com.google.common.collect.Lists;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * 调用智能合约示例
 */
@Slf4j
@SpringBootTest
class OdcContractSampleApplicationTests {

    String nodeName = "节点名称";
    String nodePrivateKey = "节点私钥";

    @Autowired
    private JsonUtils jsonUtils;

    /**
     * 创建上链对象
     *
     * @return List<ContractPayload>
     */
    List<ContractPayload> getData() {

        Dataset data1 = new Dataset();
        data1.setCstr("10000.11.001");
        data1.setName("testData001");

        Dataset data2 = new Dataset();
        data2.setCstr("10000.11.002");
        data2.setName("testData002");


        CustomPayload payload = new CustomPayload();
        payload.setNodeName(nodeName);
        payload.setPrivateKey(nodePrivateKey);
        payload.setType("test-type");
        payload.setTimestamp(System.currentTimeMillis());
        payload.setData(Lists.newArrayList(data1, data2));

        //log.debug("CustomPayload is : {}",payload);

        //log.debug("ContractPayload is : {}", ContractPayloadBuilder.build(payload));

        return ContractPayloadBuilder.build(payload);

    }


    /**
     * 调用智能合约 - 基于 webclient 发送异步请求
     *
     * @return Flux<ODCResponse>
     */
    Flux<ODCResponse> testReactiveContract() {
        List<ContractPayload> list = getData();

        log.debug("上链数据数量 is : {}", list.size());
        log.debug("ContractPayload List is : {}", list);

        // 创建 WebClient
        WebClient webClient = WebClient.builder()
                .baseUrl("https://contract.opendatachain.cn")//请求地址
                .build();
        // 发送请求并获取合约响应数据
        return webClient.post().uri("/data.upsert.batch")//与合约中路径对应
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(list)
                .retrieve()
                .bodyToFlux(ODCResponse.class);
    }

    /**
     * 单元测试方法,执行合约上链数据并打印返回结果
     *
     */
    @Test
    void testContract() {
        //调用批量数据上链方法;
        Flux<ODCResponse> results = testReactiveContract();
        // 获取并打印合约响应数据
        results.toIterable().forEach(response -> {
            log.debug("节点数据: " + response.getBody());
        });
    }

}
